package de.boeg.rdf.sansa

import net.sansa_stack.rdf.spark.io._
import org.apache.jena.graph.NodeFactory
import org.apache.jena.riot.Lang
import org.apache.jena.vocabulary.RDF
import org.apache.spark.sql.SparkSession

object TripleFilteringTest {

  def main(args: Array[String]) {
    run("src/main/resources/20171002T0930Z_BE_EQ_2.xml")
  }

  def run(input: String): Unit = {

    val spark = SparkSession.builder
      .appName(s"Triple reader example  $input")
      .master("local[*]")
      .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .getOrCreate()

    val lang = Lang.RDFXML

    val triples = spark.rdf(lang)(input)

    val acLines = triples.filter(_.getPredicate == RDF.`type`.asNode)
      .filter(_.getObject == NodeFactory.createURI("http://iec.ch/TC57/2013/CIM-schema-cim16#ACLineSegment"))
      .map(_.getSubject)

    acLines.map(l => triples.filter(t => t.getObject == l))
      .foreach(println(_))

    spark.stop
  }
}